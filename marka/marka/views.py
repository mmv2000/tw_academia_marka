from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.conf import settings
from django.urls import reverse
import json
from django.db.models import Count
from django.db.models.functions import TruncYear
from app.models import Evento, Horario

# home page.
def home(request):
    # variable de session num_visitas.
    num_visitas = request.session.get('num_visitas', 0)
    request.session['num_visitas'] = num_visitas + 1

    # default template.
    template = loader.get_template('home.html')
    context = {
        'settings': settings,
        'dataarticulos': None,
        'num_visitas': num_visitas,
    }

    # está autenticado?
    if (request.user.is_authenticated):

        # es superusuario?
        if (request.user.is_superuser):
            return HttpResponseRedirect('admin/')

        # pertenece a cierto grupo?
        if (len(request.user.groups.all()) > 0):
            # revisar el grupo (el primero).
            grupo = request.user.groups.all()[0]

            # cambia el template.
            if (grupo.name == "CoordinadorEvento"):
                template = loader.get_template('academia_marka/eventos/index.html')

            elif(grupo.name == "CoordinadorAcademia"):
                template = loader.get_template('academia_marka/horario/index.html')

                # gráfico publicaciones del usuario investigador autenticado.
                horario = Horario.objects.filter(usuario=request.user)

                # agrupa número de publicaciones por el año.
                result = (horario
                    .annotate(anio=TruncYear('fecha_hor'))
                    .values('anio')
                    .annotate(dcount=Count('anio'))
                    .order_by()
                )

                # copia a un diccionario el rsultado.
                dataevento = []
                for item in result:
                    dataevento.append({'x': item["anio"].year, 'y': item["dcount"]})

                context['dataevento'] = json.dumps(dataevento)
        else:
            # usuario sin grupo.
            pass
    else:
        # usuario no autenticado.
        pass

    return HttpResponse(template.render(context, request))

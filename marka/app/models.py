from django.contrib.auth.models import AbstractUser
from django.db import models

class Perfil(models.Model):
	nom_perfil = models.CharField(max_length = 20)

	def __str__(self):
		return self.nom_perfil

class Usuario(AbstractUser):
	email = models.EmailField('email address', unique = True)
	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['first_name', 'last_name']
	perfil = models.ForeignKey(to=Perfil, on_delete=models.PROTECT, null=True)

	def __str__(self):
		return self.first_name

class Actividad(models.Model):
	nombre_act = models.CharField('Actividad', max_length=50)
	descr = models.CharField(max_length=150, help_text='Describa la actividad')

	def __str__(self):
		return self.nombre_act

class Evento(models.Model):
	fecha_eve = models.DateField()
	ubicacion = models.CharField('Ubicación', max_length=100, help_text = 'Digite lugar del evento')
	horas = models.CharField('Duración estimada', max_length=10)
	actividad = models.ManyToManyField(to=Actividad)
	usuario = models.ForeignKey(to=Usuario, on_delete=models.PROTECT, null=True)

	class Meta:
		# para que sea una combinación única.
		unique_together = ('fecha_eve', 'ubicacion',)

	def __str__(self):
		return str(self.fecha_eve) + ' ' + self.ubicacion + ' ' + self.horas


class Profesor(models.Model):
	rut_pro = models.CharField('Rut profesor', max_length=9, unique = True)
	nom_pro = models.CharField('Nombre', max_length=20)
	ape_pro = models.CharField('Apellido', max_length=20)
	mail_pro = models.EmailField('Email')
	tel_pro = models.CharField('Teléfono', max_length=13)

	def __str__(self):
		return self.nom_pro + ' ' + self.ape_pro


class Asistente(models.Model):
	rut_alu = models.CharField('Rut alumno', max_length=9, unique = True)
	nom_alu = models.CharField('Nombre', max_length=20)
	ape_alu = models.CharField('Apellido', max_length=20)
	mail_alu = models.EmailField('Email')
	tel_alu = models.CharField('Teléfono', max_length=13)

	def __str__(self):
		return self.nom_alu + ' ' + self.ape_alu + ' ' + self.mail_alu + ' ' + self.tel_alu

class TipoBaile(models.Model):
	nom_baile = models.CharField('Baile', max_length=30)
	desc_baile = models.CharField('Descripción', max_length=150, help_text='Descripción baile')

	def __str__(self):
		return self.nom_baile

class Horario(models.Model):
	hora_ini = models.CharField('Inicio', max_length=10)
	hora_ter = models.CharField('Termino', max_length=10)
	fecha_hor = models.DateField()
	usuario = models.ForeignKey(to=Usuario, on_delete=models.SET_NULL, null=True)
	profesor = models.ForeignKey(to=Profesor, on_delete=models.SET_NULL, null=True)
	baile = models.ForeignKey(to=TipoBaile, on_delete=models.SET_NULL, null=True)
	alumno = models.ManyToManyField(to=Asistente)

	class Meta:
		unique_together = ('hora_ini', 'hora_ter', 'fecha_hor',)

	def _str_(self):
		return self.baile + ' ' + self.profesor + ' ' + self.hora_ini + ' ' + self.hora_ter + ' ' + self.fecha_hor

from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Evento, Actividad, Usuario, Horario, TipoBaile, Asistente, Profesor


class UsuarioCreationForm(UserCreationForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")


class UsuarioChangeForm(UserChangeForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")


class EventoForm(forms.ModelForm):
    class Meta:
        model = Evento
        fields = ("fecha_eve", "ubicacion", "horas",
              "actividad", "usuario")
        widgets = {
            'fecha_eve': forms.DateInput(attrs={'type': 'date', 'class': 'form.control', 'rewuiered': True}),
            'ubicacion': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'horas': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'actividad': forms.SelectMultiple(attrs={'class': 'form-control', 'required': True}),
            'coordinadorevento': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
    }

class HorarioForm(forms.ModelForm):
    class Meta:
        model = Horario
        fields = ("hora_ini", "hora_ter", "fecha_hor", "usuario", "profesor",
                "baile", "alumno")

        widgets = {
            'hora_ini': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'hora_ter': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'fecha_hor': forms.DateInput(attrs={'type': 'date', 'class': 'form.control', 'rewuiered': True}),
            'coordinadoracademia': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'profesor': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'baile': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'alumno': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
        }

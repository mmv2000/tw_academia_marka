from django.urls import path
from . import views

urlpatterns = [
    path('eventos/index/', views.index, name='index'),
    path('eventos/info/', views.info, name='info'),
    path('eventos/graficar/', views.graficar, name='graficar'),
    path('eventos/add/', views.add, name='add'),
    path('eventos/delete/<int:id>', views.delete, name='delete'),
    path('eventos/update/<int:id>', views.update, name='update'),

    path('horario/index/', views.index2, name='index2'),
    path('horario/clases/', views.horario, name='horario'),
    path('horario/info/', views.info2, name='info2'),
    #path('horario/graficar/', views.graficar2, name='graficar2'),
    path('horario/add_horario/', views.add2, name='add2'),
    path('horario/delete/<int:id>', views.delete2, name='delete2'),
    path('horario/update/<int:id>', views.update2, name='update2'),

    path('horario/profesor/index', views.index3, name='index3'),

]

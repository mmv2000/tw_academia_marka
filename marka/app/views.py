from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib import messages
import json

from django.db.models.functions import TruncYear, TruncMonth
from django.db.models import Count

from .models import Evento, Actividad, Usuario, Horario, TipoBaile, Asistente, Profesor
from .forms import EventoForm, HorarioForm

# Decorador para verificar grupo.
def es_coordinadorevento(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo Investigador?
        return user.groups.filter(name='CoordinadorEvento').exists()
    else:
        return False


# Create your views here.


@user_passes_test(es_coordinadorevento)
def index(request):
    template = loader.get_template('academia_marka/eventos/index.html')

    eventos = Evento.objects.filter(usuario=request.user)

    context = {
        'eventos': eventos,
        'settings': settings,
    }

    return HttpResponse(template.render(context, request))

@user_passes_test(es_coordinadorevento)
def info(request):
    template = loader.get_template('academia_marka/eventos/info.html')

    eventos = Evento.objects.filter(usuario=request.user)

    context = {
        'eventos': eventos,
        'settings': settings,
    }

    return HttpResponse(template.render(context, request))

@user_passes_test(es_coordinadorevento)
def graficar(request):
    template = loader.get_template('academia_marka/eventos/graficos.html')
    context = {
        'settings': settings,
        'dataevento': None,

    }

    eventos = Evento.objects.filter()

    # agrupa número de publicaciones por el año.
    result = (eventos
        .annotate(mes=TruncMonth('fecha_eve'))
        .values('mes')
        .annotate(dcount=Count('mes'))
        .order_by()
    )

    # copia a un diccionario el rsultado.
    dataevento = []
    for item in result:
        dataevento.append({'x': item["mes"].month, 'y': item["dcount"]})

    context['dataevento'] = json.dumps(dataevento)

    actividades = Evento.objects.filter()
    result = (actividades
                  .values('actividad')
                  .annotate(dcount=Count('actividad'))
                  .order_by()
                  )

    dataactividades = []
    for item in result:
        obj_rev = Actividad.objects.filter(
            id=item['actividad']).values()[0]
        dataactividades.append(
                {'label': obj_rev["nombre_act"], 'y': item["dcount"]})

    context['dataactividades'] = json.dumps(dataactividades)

    return HttpResponse(template.render(context, request))

@user_passes_test(es_coordinadorevento)
def add(request):
    if (request.method == 'GET'):
        form = EventoForm()
        # muesta usarios de esos grupos.
        form.fields['usuario'].queryset = Usuario.objects.filter(groups__name__in=['CoordinadorEvento'])

        # mostrar formulario.
        template = loader.get_template('academia_marka/eventos/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = EventoForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('info')


@user_passes_test(es_coordinadorevento)
def delete(request, id):
	eventos = Evento.objects.get(id=id)

	if (request.method == 'GET'):
		# muestra el formulario.
		template = loader.get_template('academia_marka/eventos/delete.html')

		context = {
            'settings': settings,
            'eventos': eventos,
        }
		return HttpResponse(template.render(context, request))

	elif (request.method == 'POST'):
		# elimina la publicación.
		eventos.delete()
		messages.success(request, "Eliminación realizada.")

		return redirect('index')

@user_passes_test(es_coordinadorevento)
def update(request, id):
	eventos = Evento.objects.get(id=id)

	if (request.method == 'GET'):
		# muestra el formulario con datos.
		form = EventoForm(instance=eventos)
		form.fields['usuario'].queryset = Usuario.objects.filter(groups__name__in=['CoordinadorEvento'])
		template = loader.get_template('academia_marka/eventos/add.html')

		context = {
            'eventos': eventos,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

		return HttpResponse(template.render(context, request))

	elif (request.method == 'POST'):
		# actualiza datos.
		form = EventoForm(request.POST, instance=eventos)

		if form.is_valid():
			form.save()
			messages.success(request, "Actualización realizada.")

		return redirect('index')

# COORDINADOR ACADEMIA
def es_coordinadoracademia(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo Investigador?
        return user.groups.filter(name='CoordinadorAcademia').exists()
    else:
        return False

@user_passes_test(es_coordinadoracademia)
def index2(request):
    template = loader.get_template('academia_marka/horario/index.html')

    horarios = Horario.objects.all()

    context = {
        'horarios': horarios,
        'settings': settings,
    }

    return HttpResponse(template.render(context, request))

@user_passes_test(es_coordinadoracademia)
def info2(request):
    template = loader.get_template('academia_marka/horario/info.html')

    horarios = Horario.objects.all()

    context = {
        'horarios': horarios,
        'settings': settings,
    }

    return HttpResponse(template.render(context, request))

@user_passes_test(es_coordinadoracademia)
def add2(request):
    if (request.method == 'GET'):
        form = HorarioForm()
        # muesta usarios de esos grupos.
        form.fields['usuario'].queryset = Usuario.objects.filter(groups__name__in=['CoordinadorAcademia'])

        # mostrar formulario.
        template = loader.get_template('academia_marka/horario/add_horario.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = HorarioForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('info2')


@user_passes_test(es_coordinadoracademia)
def delete2(request, id):
	horarios = Horario.objects.get(id=id)

	if (request.method == 'GET'):
		# muestra el formulario.
		template = loader.get_template('academia_marka/horario/delete.html')

		context = {
            'settings': settings,
            'horarios': horarios,
        }
		return HttpResponse(template.render(context, request))

	elif (request.method == 'POST'):
		# elimina la publicación.
		eventos.delete()
		messages.success(request, "Eliminación realizada.")

		return redirect('index')

@user_passes_test(es_coordinadoracademia)
def update2(request, id):
	horarios = Horario.objects.get(id=id)

	if (request.method == 'GET'):
		# muestra el formulario con datos.
		form = HorarioForm(instance=horarios)
		form.fields['usuario'].queryset = Usuario.objects.filter(groups__name__in=['CoordinadorAcademia'])
		template = loader.get_template('academia_marka/horario/add_horario.html')

		context = {
            'horarios': horarios,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

		return HttpResponse(template.render(context, request))

	elif (request.method == 'POST'):
		# actualiza datos.
		form = HorarioForm(request.POST, instance=horarios)

		if form.is_valid():
			form.save()
			messages.success(request, "Actualización realizada.")

		return redirect('index')

@user_passes_test(es_coordinadoracademia)
def index3(request):
    template = loader.get_template('academia_marka/horario/profesor/index.html')

    profesores = Profesor.objects.all()

    context = {
        'profesores': profesores,
        'settings': settings,
    }

    return HttpResponse(template.render(context, request))

@user_passes_test(es_coordinadoracademia)
def horario(request):
    template = loader.get_template('academia_marka/horario/clases.html')

    context = {
        'settings': settings
    }
    # print(ejemplares[0]["disponible"])
    # print(ejemplares[0]["disponible"])
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    return HttpResponse(template.render(context, request))

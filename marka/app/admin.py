from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import UsuarioCreationForm, UsuarioChangeForm
from .models import Perfil, Usuario, Evento, Actividad, Horario, Profesor, Asistente, TipoBaile

class UsuarioAdmin(UserAdmin):
    add_form = UsuarioCreationForm
    form = UsuarioChangeForm
    model = Usuario
    list_display = ["email", "username", "first_name", "last_name"]

class EventoAdmin(admin.ModelAdmin):
	list_display = ('fecha_eve', 'ubicacion', 'horas')
	search_fields = ('fecha_eve',)
	list_filter = ('fecha_eve', 'ubicacion',)

class HorarioAdmin(admin.ModelAdmin):
	list_display = ('baile', 'profesor', 'hora_ini', 'hora_ter')
	search_fields = ('baile',)
	list_filter = ('baile', 'profesor',)

class ProfesorAdmin(admin.ModelAdmin):
	list_display = ('nom_pro', 'ape_pro', 'mail_pro', 'tel_pro')
	search_fields = ('nom_pro',)

class AsistenteAdmin(admin.ModelAdmin):
	list_display = ('nom_alu', 'ape_alu', 'mail_alu', 'tel_alu')
	search_fields = ('nom_alu',)

admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Perfil)
admin.site.register(Evento, EventoAdmin)
admin.site.register(Actividad)
admin.site.register(Horario, HorarioAdmin)
admin.site.register(Profesor, ProfesorAdmin)
admin.site.register(Asistente, AsistenteAdmin)
admin.site.register(TipoBaile)
